﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlineShoppingStore.Domain.Abstract;
using OnlineShoppingStore.Domain.Entities;
using System.Net;
using System.Net.Mail;

//using EASendMail;

namespace OnlineShoppingStore.Domain.Concrete
{
    public class EmailOrderProcessor : IOrderProcessor
    {
        private EmailSettings emailSettings;

        public EmailOrderProcessor(EmailSettings settings)
        {
            emailSettings = settings;
        }

        public void ProcessOrder(Cart cart, ShippingDetails shippingInfo)
        {
            using (var smtpClient = new SmtpClient())
            //using (var smtpClient = new SmtpMail())
            {
                //smtpClient.EnableSsl = emailSettings.UseSsl;
                //smtpClient.Host = emailSettings.ServerName;
                //smtpClient.Port = emailSettings.ServerPort;
                //smtpClient.UseDefaultCredentials = false;
                smtpClient.Host = emailSettings.ServerName;  //smtp.gmail.com
                smtpClient.Port = emailSettings.ServerPort;  //587
                smtpClient.EnableSsl = emailSettings.UseSsl; //true
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;

                smtpClient.Credentials = new NetworkCredential(emailSettings.UserName, emailSettings.Password);

                StringBuilder body = new StringBuilder()
                    .AppendLine("A new order has been submitted")
                    .AppendLine("---")
                    .AppendLine("Items:");
                foreach (var line in cart.Lines)
                {
                    var subtotal = line.Product.Price * line.Quantity;
                    body.AppendFormat("{0} x {1} (subtotal: {2:c})\n",
                        line.Quantity,
                        line.Product.Name,
                        subtotal);
                }
                body.AppendFormat("Total order value: {0:c}",
                    cart.ComputeTotalValue())
                    .AppendLine("---")
                    .AppendLine("Ship to:")
                    .AppendLine(shippingInfo.Name)
                    .AppendLine(shippingInfo.Line1)
                    .AppendLine(shippingInfo.Line2 ?? "")
                    .AppendLine(shippingInfo.Line3 ?? "")
                    .AppendLine(shippingInfo.City)
                    .AppendLine(shippingInfo.State ?? "")
                    .AppendLine(shippingInfo.Country)
                    .AppendLine(shippingInfo.Zip)
                    .AppendLine("---")
                    .AppendFormat("Gift wrap: {0}",
                        shippingInfo.GiftWrap ? "Yes:" : "No");
                MailMessage mailMessage = new MailMessage(
                   
                    emailSettings.MailFromAddress,
                    emailSettings.MailToAddress,
                    "New order submitted!",
                    body.ToString()); 
               
                //Sends the specified message to an SMTP server for delivery
                smtpClient.Send(mailMessage);
                //smtpClient.Credentials = new NetworkCredential("michael.nhong@gmail.com", "cbraxydruwcqqilb");
            }
        }
    }
}
