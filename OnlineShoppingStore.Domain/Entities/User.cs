﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShoppingStore.Domain.Entities
{
    public class User
    {
        //NOTES: the data innotation, if class name is User and the property name is classname and with Id
        // this will automatically set value in the database table as PRIMARY KEY with the [Key] innotation, otherwise
        // use the [Key] innotation
        [Key]
        public string UserId { get; set; }
        public string Password { get; set; }
    }
}
