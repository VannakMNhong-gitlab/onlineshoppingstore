﻿using OnlineShoppingStore.Domain.Abstract;
using OnlineShoppingStore.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace OnlineShoppingStore.WebUI.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        //From the Ninject Plugin
        IAuthentication authentication;
        public AccountController(IAuthentication authentication)
        {
            //This authentication means IAuthentication and it is equal to the parameter authentication
            this.authentication = authentication;
        }
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (authentication.Authenicate(model.UserName, model.Password))
                {
                    //Add user name in Cookies
                    FormsAuthentication.SetAuthCookie(model.UserName, false);
                    //redirect to admin index...
                    return Redirect(returnUrl ?? Url.Action("Index", "Admin"));
                }
                else
                {
                    ModelState.AddModelError("", "Incorrect user or password");
                    return View();
                }
            }
            else
            {
                return View();
            }          
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Admin");
        }

        //Change Password
        //Forget Password

    }
}