﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OnlineShoppingStore.Domain.Abstract;
using OnlineShoppingStore.Domain.Entities;
using OnlineShoppingStore.WebUI.Models;

namespace OnlineShoppingStore.WebUI.Controllers
{
#pragma warning disable CS0246 // The type or namespace name 'Controller' could not be found (are you missing a using directive or an assembly reference?)
    public class CartController : Controller
#pragma warning restore CS0246 // The type or namespace name 'Controller' could not be found (are you missing a using directive or an assembly reference?)
    {
        private IProductRepository repository;
        private IOrderProcessor orderProcessor;
        public CartController(IProductRepository repo, IOrderProcessor proc)
        {
            repository = repo;
            orderProcessor = proc;
        }

#pragma warning disable CS0246 // The type or namespace name 'ViewResult' could not be found (are you missing a using directive or an assembly reference?)
        public ViewResult Index(Cart cart, string returnUrl)
#pragma warning restore CS0246 // The type or namespace name 'ViewResult' could not be found (are you missing a using directive or an assembly reference?)
        {
            return View(
                new CartIndexViewModel { Cart = cart, ReturnUrl = returnUrl});
        }
        
#pragma warning disable CS0246 // The type or namespace name 'PartialViewResult' could not be found (are you missing a using directive or an assembly reference?)
        public PartialViewResult Summary(Cart cart)
#pragma warning restore CS0246 // The type or namespace name 'PartialViewResult' could not be found (are you missing a using directive or an assembly reference?)
        {
            return PartialView(cart);
        }
        //this HttpGet - if not mention attribute, it will be HttpGet
#pragma warning disable CS0246 // The type or namespace name 'ViewResult' could not be found (are you missing a using directive or an assembly reference?)
        public ViewResult Checkout()
#pragma warning restore CS0246 // The type or namespace name 'ViewResult' could not be found (are you missing a using directive or an assembly reference?)
        {
            return View(new ShippingDetails());
        }
#pragma warning disable CS0246 // The type or namespace name 'HttpPostAttribute' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'HttpPost' could not be found (are you missing a using directive or an assembly reference?)
        [HttpPost]
#pragma warning restore CS0246 // The type or namespace name 'HttpPost' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning restore CS0246 // The type or namespace name 'HttpPostAttribute' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'ViewResult' could not be found (are you missing a using directive or an assembly reference?)
        public ViewResult Checkout(Cart cart, ShippingDetails shippingDetails)
#pragma warning restore CS0246 // The type or namespace name 'ViewResult' could not be found (are you missing a using directive or an assembly reference?)
        {
            if (cart.Lines.Count() == 0)
            {
                ModelState.AddModelError("", "Sorry, your cart is empty!");
            }

            if (ModelState.IsValid)
            {
                orderProcessor.ProcessOrder(cart, shippingDetails);
                cart.Clear();
                return View("Completed");
            }
            else
            {
                return View(new ShippingDetails());
            }

            
        }

#pragma warning disable CS0246 // The type or namespace name 'RedirectToRouteResult' could not be found (are you missing a using directive or an assembly reference?)
        public RedirectToRouteResult AddToCart(Cart cart, int productId, string returnUrl)
#pragma warning restore CS0246 // The type or namespace name 'RedirectToRouteResult' could not be found (are you missing a using directive or an assembly reference?)
        {
            Product product = repository.Products.FirstOrDefault(p => p.ProductId == productId);
            if (product != null)
            {
                //add product to cart - will need to create helper method
                cart.AddItem(product, 1);
            }

            return RedirectToAction("Index", new {returnUrl});
        }

#pragma warning disable CS0246 // The type or namespace name 'RedirectToRouteResult' could not be found (are you missing a using directive or an assembly reference?)
        public RedirectToRouteResult RemoveFromCart(Cart cart, int productId, string returnUrl)
#pragma warning restore CS0246 // The type or namespace name 'RedirectToRouteResult' could not be found (are you missing a using directive or an assembly reference?)
        {
            Product product = repository.Products.FirstOrDefault(p => p.ProductId == productId);
            if (product != null)
            {
                cart.RemoveLine(product);
            }

            return RedirectToAction("Index", new {returnUrl});
        }
    }
}