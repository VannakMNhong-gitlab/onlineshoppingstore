﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineShoppingStore.Domain.Abstract;

namespace OnlineShoppingStore.WebUI.Controllers
{
#pragma warning disable CS0246 // The type or namespace name 'Controller' could not be found (are you missing a using directive or an assembly reference?)
    public class NavController : Controller
#pragma warning restore CS0246 // The type or namespace name 'Controller' could not be found (are you missing a using directive or an assembly reference?)
    {
        private IProductRepository repository;

        public NavController(IProductRepository repo)
        {
            repository = repo;
        }
        //the manu will return partial result
#pragma warning disable CS0246 // The type or namespace name 'PartialViewResult' could not be found (are you missing a using directive or an assembly reference?)
        public PartialViewResult Menu(string category = null)
#pragma warning restore CS0246 // The type or namespace name 'PartialViewResult' could not be found (are you missing a using directive or an assembly reference?)
        {
            //pass this cagetory to the view
            ViewBag.SelectedCategory = category;
            //select from the database ienumerable 
            IEnumerable<string> categories = repository.Products
                .Select(x => x.Category)
                .Distinct()
                .OrderBy(x => x);
            return PartialView(categories);
        }
    }
}